#include <time.h>
#include <stdlib.h>
#include <Wire.h>

#include "U8glib.h"

U8GLIB_SH1106_128X64 u8g(10, 9, 12, 11, 8);

int RED = 3, GREEN = 4, BLUE = 5;

uint8_t incomingByte = 0;

const int MPU = 0x68;

// Accelerometer X, Y, and Z values
int16_t AcX[12];
int16_t AcY[12];
int16_t AcZ[12];

// Gyroscope X, Y, and Z values
int16_t GyX[12];
int16_t GyY[12];
int16_t GyZ[12];


uint8_t SampleRate = 10;

// Temperature value
int16_t temperature = 0;

void setup() {
  clearDisplay();
  u8g.firstPage();  
  do {
    logo();
  u8g.setColorIndex(1);
  } while( u8g.nextPage() );
  delay(1000);
  u8g_prepare();
  Serial.begin(9600);
  Serial.flush();
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
  digitalWrite(RED, HIGH);
  digitalWrite(BLUE, HIGH);
  digitalWrite(GREEN, HIGH);
  // Start sensor
  Wire.begin();
  Wire.beginTransmission( MPU );
  Wire.write( 0x6B ); 
  Wire.write( 0 );    
  Wire.endTransmission( true );
  Serial.println("Program started");
}

void loop() {
  if(Serial.available() > 0){
    incomingByte = Serial.read();
    switch (incomingByte){
      case 'r':
        digitalWrite(RED, LOW);
        break;
      case 'g':
        digitalWrite(GREEN, LOW);
        break;
      case 'b':
        digitalWrite(BLUE, LOW);
        break;
      case 'o':
        for(uint8_t i = 3; i <= 5; i += 1){
          digitalWrite(i, HIGH);
        }
        break;
      case 's':
        Wire.beginTransmission( MPU );
        Wire.write( 0x3B );  
        Wire.endTransmission( false );
        Wire.requestFrom( MPU, 14, true );  

        temperature = Wire.read() << 8 | Wire.read();
        temperature = (temperature/340) + 36.53;
  
        for(int i = 0; i < SampleRate; i++){
          GyX[i] = Wire.read() << 8 | Wire.read();  
          GyY[i] = Wire.read() << 8 | Wire.read();  
          GyZ[i] = Wire.read() << 8 | Wire.read(); 

          AcX[i] = Wire.read() << 8 | Wire.read();    
          AcY[i] = Wire.read() << 8 | Wire.read();  
          AcZ[i] = Wire.read() << 8 | Wire.read();

          delay(1000); 
        }

        GyX[SampleRate] = 0;
        GyY[SampleRate] = 0;
        GyZ[SampleRate] = 0;

        AcX[SampleRate] = 0;
        AcY[SampleRate] = 0;
        AcZ[SampleRate] = 0;
        for(int i = 0; i < 3; i++){
          GyX[SampleRate] += GyX[i];
          GyY[SampleRate] += GyY[i];
          GyZ[SampleRate] += GyZ[i];

          AcX[SampleRate] += AcX[i];
          AcY[SampleRate] += AcY[i];
          AcZ[SampleRate] += AcZ[i];
        } 
        GyX[SampleRate] /= 3;
        GyY[SampleRate] /= 3;
        GyZ[SampleRate] /= 3;

        Serial.println( "Gyroscope:" );  
        Serial.println( GyX[SampleRate] );
        Serial.println( GyY[SampleRate] );
        Serial.println( GyZ[SampleRate] );

        Serial.println( "Accelerometer:" );  
        Serial.println( AcX[SampleRate] );
        Serial.println( AcY[SampleRate] );
        Serial.println( AcZ[SampleRate] );

        Serial.println("Temperature:");
        Serial.println(temperature);
        delay( 3000 );

        break;
      //This is for asthetics :-)
      case 'q':
        Serial.println("Goodbye");
      //Default is do nothing, edge case will be caught in Python
      default:
        break;
    }
  }
}

void u8g_prepare(void) {
  u8g.setFont(u8g_font_6x10);
  u8g.setFontRefHeightExtendedText();
  u8g.setDefaultForegroundColor();
  u8g.setFontPosTop();
}

void logo(){
  u8g.drawBox(32, 0, 64, 10);
  u8g.drawBox(86, 0, 10, 64);
  u8g.drawBox(54, 28, 32, 10);
}

void clearDisplay(){
  digitalWrite(8, LOW);
  delay(100);
  digitalWrite(8, HIGH);
}
